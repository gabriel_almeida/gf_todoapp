package com.example.myapplication

import android.annotation.SuppressLint
import androidx.annotation.IdRes
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.example.myapplication.classes.Task

class TaskList (
    private val context: Context,
    @IdRes private val containerId: Int,
    private val tasks: ArrayList<Task>) {

    private val container: LinearLayout = (this.context as Activity).findViewById(this.containerId)
    private var lastListSize: Int = this.tasks.size

    fun build(): TaskList {
        this.show()
        return this
    }

    fun update(forceUpdate: Boolean = false): TaskList {
        if (forceUpdate || lastListSize != this.tasks.size) {
            this.show()
            this.lastListSize = this.tasks.size
        }
        return this
    }

    @SuppressLint("ResourceAsColor")
    fun show() {
        this.container.removeAllViews()
        for (task in tasks) {
            val v: View = View.inflate(this.context, R.layout.task_card, null)
            val taskDescription: TextView = v.findViewById(R.id.taskLabel)
            val taskUrgency: LinearLayout = v.findViewById(R.id.taskDoneColor)
            val taskStatus: CheckBox = v.findViewById(R.id.taskStatusCheckbox)
            taskStatus.tag = task
            taskStatus.setOnCheckedChangeListener{v, isChecked ->
                val task = v.tag as Task
                task.taskStatus = isChecked
            }
            if (task.taskStatus) {
                taskStatus.isChecked = true
            }
            if (task.taskUrgency) {
                taskUrgency.setBackgroundColor(Color.RED)
            } else {
                taskUrgency.setBackgroundColor(Color.GREEN)
            }
            taskDescription.text = task.taskDescription
            this.container.addView(v)
        }
    }

    fun showDone() {
        this.container.removeAllViews()
        for (task in tasks) {
            if (!task.taskStatus) {
                val v: View = View.inflate(this.context, R.layout.task_card, null)
                val taskDescription: TextView = v.findViewById(R.id.taskLabel)
                val taskUrgency: LinearLayout = v.findViewById(R.id.taskDoneColor)
                val taskStatus: CheckBox = v.findViewById(R.id.taskStatusCheckbox)
                taskStatus.tag = task
                taskStatus.setOnCheckedChangeListener { v, isChecked ->
                    val task = v.tag as Task
                    task.taskStatus = isChecked
                }
                if (task.taskStatus) {
                    taskStatus.isChecked = true
                }
                if (task.taskUrgency) {
                    taskUrgency.setBackgroundColor(Color.RED)
                } else {
                    taskUrgency.setBackgroundColor(Color.GREEN)
                }
                taskDescription.text = task.taskDescription
                this.container.addView(v)
            }
        }
    }
}