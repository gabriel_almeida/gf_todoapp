package com.example.myapplication.classes

class Task {
    var taskDescription: String = ""
    var taskUrgency: Boolean = false
    var taskStatus: Boolean = false

    constructor(taskDescription: String, taskUrgency: Boolean, taskStatus: Boolean){
        this.taskDescription = taskDescription;
        this.taskUrgency = taskUrgency;
        this.taskStatus = taskStatus;
    }
}