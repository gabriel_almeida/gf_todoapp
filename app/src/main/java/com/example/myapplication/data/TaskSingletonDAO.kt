package com.example.myapplication.data
import com.example.myapplication.classes.Task

object TaskSingletonDAO {

    private var taskList: ArrayList<Task> = ArrayList()

    fun add(task: Task) {
        this.taskList.add(task)
    }

    fun getTasks(): ArrayList<Task> {
        return this.taskList
    }

}