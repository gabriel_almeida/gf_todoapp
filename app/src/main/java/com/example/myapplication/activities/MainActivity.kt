package com.example.myapplication.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.Switch
import android.widget.TextView
import com.example.myapplication.R
import com.example.myapplication.TaskList
import com.example.myapplication.classes.Task
import com.example.myapplication.data.TaskSingletonDAO

class MainActivity : AppCompatActivity() {

    private lateinit var taskList: TaskList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.taskList = TaskList(this, R.id.taskLinearLayout, TaskSingletonDAO.getTasks())
        val showNotDoneTasks: CheckBox = findViewById(R.id.showNotDone)
        showNotDoneTasks.setOnCheckedChangeListener{_, isChecked ->
            if(isChecked){
                this.taskList.showDone()
            }else{
                this.taskList.show()
            }
        }
    }

    fun onClickOk(v: View) {
        var fieldTaskReceiver: TextView = findViewById(R.id.fieldTask)
        var switchUrgency: Switch = findViewById(R.id.switchUrgency)
        if (fieldTaskReceiver.text.isNotEmpty()) {
            var task: Task
            if (switchUrgency.isChecked) {
                task = Task(
                    taskDescription = fieldTaskReceiver.text.toString(),
                    taskUrgency = true,
                    taskStatus = false
                )
            } else {
                task = Task(
                    taskDescription = fieldTaskReceiver.text.toString(),
                    taskUrgency = false,
                    taskStatus = false
                )
            }
            TaskSingletonDAO.add(task)
            this.taskList.update()
        }
    }
}